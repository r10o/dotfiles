#My Configuration

This repository contains my configuration files for the following:

* Vim
* bspwm
* sxhkd
* Xterm/Urxvt
* git
* zsh
* X display server
